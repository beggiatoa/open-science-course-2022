# Opening Science Course 2022
#### Opening your research to collaborative futures

Course organized by the following institutes:

- University College Dublin (Ireland) and Insight SFI Research Centre for Data Analytics
- Aarhus University (Denmark)
- Maynooth University (Ireland)
- National Training Fund (Czech Republic)
- Research Institute for Humanity and Nature (Japan)

Further course information are available at
https://openingdoors4phd.eu/online_course.html.

The course schedule is available in this repository: [PDF](./documentation/schedule.pdf)

---

## Objectives

Attend the course and complete the assignments to obtain the ECTS and the course
certificate.

## Project status

- **2022-01-12 Wed.**  Course start.

## Reflection
## Authors

 - Marco Prevedello <m.prevedello1@nuigalway.ie>; [Preve92](https://twitter.com/Preve92)

## Version history

- **0.1.0**: You have to start somewhre.

## Acknowledgments
## License

The content of this repository is by Marco Prevedello©.

All written documents are licensed under a Creative Commons
Attribution-ShareAlike 4.0 International License.  You should have received a
copy of the license along with this work. If not, see
<http://creativecommons.org/licenses/by-sa/4.0/>.

All source code files are to be considered free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.

## Bibliography
